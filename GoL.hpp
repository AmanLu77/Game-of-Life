#include <SFML/Graphics.hpp>
#include <iostream>
#include <ctime>
#include <vector>
#include <random>
#include <windows.h>

#define msleep(x) Sleep(x)
#define N 25

//using namespace sf;
using namespace std;

class Point {
public:
    Point();
    ~Point();

    unsigned is_live = 1;

    sf::RectangleShape Get();
    void SetColorBlue();
    void SetColorWhite();
    void SetColorGreen();
    void SetColorRed();
    void SetX(int x);
    void SetY(int y);
    const int GetX();
    const int GetY();
    //const float GetCellWidth();
    //const float GetCellHeight();
    void SetColor(int i1, int i2, int i3);

private:
    int m_x, m_y;
    float m_r;
    sf::RectangleShape m_rectangle;
};

//��������� ��������� ������� ����� (����������� ���� 1 �������)
void point_neighbors_coord(signed int nb[][2], unsigned int x, unsigned int y);

//���������� ����� ������� � ������ � ������������ x, y
unsigned int count_live_neighbors(Point world[][N], unsigned int x, unsigned int y);

//������� �� ����� ������� ����
void print_world(Point world[][N]);

//������������� ��������� ��������� �������� ����
void next_generation(Point world[][N], Point prev_world[][N]);

//����������� �������� ����. ������������ ��� ���������� ����������� ���������
void copy_world(Point src[][N], Point dest[][N]);

//��������� ������� ����� �������� � ����������� ���������
int cmp_world(Point w1[][N], Point w2[][N]);

//���������� ����� ������ �� ������� ����
unsigned int get_live_count(Point world[][N]);

void WinnerText(sf::RenderWindow &window);

void GameOverText(sf::RenderWindow &window);