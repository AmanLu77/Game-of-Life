#include "GoL.hpp"

int main()
{
    srand(time(0));
    Point cells[N][N];
    int Mode;
    int M;

    std::cout << "Please choose game mode: \n0 - Automatic, \n1 - Interactive:\n";
    std::cin >> Mode;

    if (Mode == 1)
    {
        std::cout << "Enter the number of points you want to place:\n";
        std::cin >> M;
    }

    if (Mode == 0)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                // ��������� ��������� �����
                if (rand() % 4 == 0)
                {
                    cells[i][j].is_live = 1;
                    cells[i][j].SetColorBlue();
                }
                else
                {
                    cells[i][j].is_live = 0;
                }
                cells[i][j].SetX(((N) * j) + N);
                cells[i][j].SetY(((N) * i) + N);
            }
        }
    }
    //print_world(cells);

    const int width = 700;
    const int height = 700;
    Point prev_world[N][N];
    Point prev_prev_world[N][N];

    int Alive = 0;
    unsigned int live_points = 0;
    bool is_optimal = false;
    
    sf::RenderWindow My_window(sf::VideoMode(width, height), "Game of Life");
    My_window.clear(sf::Color(130, 130, 130, 0));

    if (Mode == 1)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                cells[i][j].is_live = 0;

                cells[i][j].SetX(((N)*j) + N);
                cells[i][j].SetY(((N)*i) + N);

                My_window.draw(cells[i][j].Get());
            }
        }
        My_window.display();

        while (Alive != M)
        {
            sf::Event ev;
            while (My_window.pollEvent(ev))
            {
                if (ev.type == sf::Event::Closed)
                    My_window.close();

                if (ev.type == sf::Event::MouseButtonPressed && ev.mouseButton.button == sf::Mouse::Left())
                {
                    float mouse_x = ev.mouseButton.x;
                    float mouse_y = ev.mouseButton.y;

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < N; j++)
                        {
                            if (cells[i][j].Get().getGlobalBounds().contains(mouse_x, mouse_y))
                            {
                                cells[i][j].is_live = 1;
                                cells[i][j].SetColorGreen();
                                Alive++;
                            }
                            My_window.draw(cells[i][j].Get());
                        }
                    }
                    My_window.display();
                }
            }
        }
    }

    // ������
    while (My_window.isOpen())
    {

        sf::Event event;
        while (My_window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                My_window.close();
        }
        //print_world(cells);

        copy_world(prev_world, prev_prev_world);
        copy_world(cells, prev_world);

        next_generation(cells, prev_world);
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (cells[i][j].is_live == 1) {
                    cells[i][j].SetColorBlue();
                }
                else{
                    cells[i][j].SetColorWhite();
                }
            }
        }
        is_optimal = cmp_world(cells, prev_world) == 0;
        is_optimal = cmp_world(cells, prev_prev_world) == 0;
        live_points = get_live_count(cells);

        if (is_optimal)
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (cells[i][j].is_live == 1) {
                        cells[i][j].SetColorGreen();
                    }
                    else {
                        cells[i][j].SetColorWhite();
                    }
                }
            }
        }

        if (live_points == 0)
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    cells[i][j].SetColor(200, 20, 20);
                }
            }
        }
        msleep(100);

        My_window.clear(sf::Color(130, 130, 130, 0));
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                My_window.draw(cells[i][j].Get());
            }
        }

        if (live_points == 0) {
            GameOverText(My_window);
        }
        if (is_optimal && live_points != 0) {
            WinnerText(My_window);
        }

        My_window.display();
    }
    return 0;
}