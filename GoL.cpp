#include "GoL.hpp"

// Class Point
Point::Point()
{
    m_y = 0;
    m_x = 0;
    m_r = 20;
    m_rectangle = sf::RectangleShape(sf::Vector2f(m_r, m_r));
    m_rectangle.setOrigin(m_r, m_r);
    m_rectangle.setPosition(m_x, m_y);
    m_rectangle.setFillColor(sf::Color::White);
}

Point::~Point() {   }

sf::RectangleShape Point::Get()
{
    return m_rectangle;
}
void Point::SetColorBlue()
{
    m_rectangle.setFillColor(sf::Color::Blue);
}
void Point::SetColorWhite()
{
    m_rectangle.setFillColor(sf::Color::White);
}
void Point::SetColorGreen()
{
    m_rectangle.setFillColor(sf::Color::Green);
}
void Point::SetColorRed()
{
    m_rectangle.setFillColor(sf::Color::Red);
}
void Point::SetX(int x)
{
    m_x = x;
    m_rectangle.setPosition(m_x, m_y);
}
void Point::SetY(int y)
{
    m_y = y;
    m_rectangle.setPosition(m_x, m_y);
}
const int Point::GetX()
{
    return m_rectangle.getPosition().x;
}
const int Point::GetY()
{
    return m_rectangle.getPosition().y;
}
/*
const float Point::GetCellWidth()
{
    float Width = m_rectangle.getPosition().x + m_r;
    return Width;
}
const float Point::GetCellHeight()
{
    float Height = m_rectangle.getPosition().y + m_r;
    return Height;
}
*/
void Point::SetColor(int i1, int i2, int i3)
{
    m_rectangle.setFillColor(sf::Color(i1, i2, i3));
}

//Functions
//��������� ��������� ������� ����� (����������� ���� 1 �������)
void point_neighbors_coord(signed int nb[][2], unsigned int x, unsigned int y)
{
    unsigned int i, j;
    unsigned int k = 0;

    for (i = x - 1; i <= x + 1; i++) {
        for (j = y - 1; j <= y + 1; j++) {
            if (i == x && j == y) {
                continue;
            }
            nb[k][0] = i;
            nb[k][1] = j;
            k++;
        }
    }
}

//���������� ����� ������� � ������ � ������������ x, y
unsigned int count_live_neighbors(Point world[][N], unsigned int x, unsigned int y)
{
    unsigned int count = 0;
    unsigned int i;
    signed int nb[8][2];
    signed int _x, _y;

    point_neighbors_coord(nb, x, y);

    for (i = 0; i < 8; i++) {
        _x = nb[i][0];
        _y = nb[i][1];

        // ���� ����� ��� �������� ����
        if (_x < 0 || _y < 0) {
            continue;
        }
        // ���� ����� ��� �������� ����
        if (_x >= N || _y >= N) {
            continue;
        }
        // ���� ����� �����
        if (world[_x][_y].is_live == 1) {
            count++;
        }
    }

    return count;
}

//������� �� ������� ������� ����
void print_world(Point world[][N])
{
    unsigned int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if (world[i][j].is_live == 1) {
                cout << '*';
            }
            else {
                cout << ' ';
            }
            cout << ' ';
        }
        cout << endl;
    }
}

//������������� ��������� ��������� �������� ����
void next_generation(Point world[][N], Point prev_world[][N])
{
    unsigned int i, j;
    unsigned int live_nb;
    Point p;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            p = prev_world[i][j];
            live_nb = count_live_neighbors(prev_world, i, j);

            if (p.is_live == 0) {
                if (live_nb == 3) {
                    world[i][j].is_live = 1;
                }
            }
            else {
                if (live_nb < 2 || live_nb > 3) {
                    world[i][j].is_live = 0;
                    world[i][j].SetColorWhite();
                }
            }
        }
    }
}

//����������� �������� ����. ������������ ��� ���������� ����������� ���������
void copy_world(Point src[][N], Point dest[][N])
{
    unsigned int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            dest[i][j] = src[i][j];
        }
    }
}

//��������� ������� ����� �������� � ����������� ���������
int cmp_world(Point w1[][N], Point w2[][N])
{
    unsigned int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if (w1[i][j].is_live != w2[i][j].is_live) {
                return -1;
            }
        }
    }
    return 0;
}

//���������� ����� ������ �� ������� ����
unsigned int get_live_count(Point world[][N])
{
    unsigned int count = 0;
    unsigned i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if (world[i][j].is_live == 1) {
                count++;
            }
        }
    }
    return count;
}

void WinnerText(sf::RenderWindow &window)
{
    sf::Font font;
    if (!font.loadFromFile("Fonts/PixellettersFull.ttf"))
    {
        cout << "Error loading font!" << '\n';
    }
    sf::Text WinnerText;
    WinnerText.setFont(font);
    WinnerText.setFillColor(sf::Color::Green);
    WinnerText.setOutlineColor(sf::Color::Black);
    WinnerText.setOutlineThickness(2.f);
    WinnerText.setString("Optimal configuration detected!");
    WinnerText.setCharacterSize(40);
    WinnerText.setPosition(
        window.getSize().x / 2.f - WinnerText.getGlobalBounds().width / 2.f,
        window.getSize().y / 2.f - WinnerText.getGlobalBounds().height / 2.f);

    window.draw(WinnerText);
}

void GameOverText(sf::RenderWindow &window)
{
    sf::Font font;
    if (!font.loadFromFile("fonts/PixellettersFull.ttf"))
    {
        cout << "Error loading font!" << '\n';
    }
    sf::Text GameOverText;
    GameOverText.setFont(font);
    GameOverText.setFillColor(sf::Color::Red);
    GameOverText.setOutlineColor(sf::Color::White);
    GameOverText.setOutlineThickness(2.f);
    GameOverText.setString("All points died!");
    GameOverText.setCharacterSize(60);
    GameOverText.setPosition(
        window.getSize().x / 2.f - GameOverText.getGlobalBounds().width / 2.f,
        window.getSize().y / 2.f - GameOverText.getGlobalBounds().height / 2.f);

    window.draw(GameOverText);
}